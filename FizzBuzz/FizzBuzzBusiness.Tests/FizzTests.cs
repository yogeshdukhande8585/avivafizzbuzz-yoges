﻿//-----------------------------------------------------------------------
// <copyright file="FizzTests.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzz.Controllers;
    using FizzBuzz.Models;
    using FizzBuzz.Repository;
    using FizzBuzzBusinessLayer;
    using Moq;
    using NUnit.Framework;
    using StructureMap;
    using StructureMap.Graph;

    /// <summary>
    /// Definition of FizzTests Class
    /// </summary>
    [TestFixture]
    public class FizzTests
    {
        /// <summary>
        /// Define Container
        /// </summary>
        private IContainer container;

        /// <summary>
        /// if number is divisible by 3 then replace with proper message
        /// </summary>
        [Test]
        public void Should_Show_Proper_Message_MessageColor()
        {
            var repoResponse = new List<int>();

            for (var counter = 1; counter <= 20; counter++)
            {
                repoResponse.Add(counter);
            }

            var mockRepository = new Mock<IRepository>();

            mockRepository.Setup(x => x.GetNumbers(It.IsAny<int>())).Returns(repoResponse);

            IEnumerable<BaseFizzBuzz> fizzBuzzinstances = this.container.GetAllInstances<BaseFizzBuzz>();
            var mockFizzBuzzBusiness = new Mock<FizzBuzzBusinessLogic>();
            var controller = new FizzBuzzController(mockRepository.Object, fizzBuzzinstances, mockFizzBuzzBusiness.Object);

            var result = controller.FizzBuzzView(new FizzBuzzModel { Number = 20 }) as ViewResult;

            var viewModel = (FizzBuzzModel)result.Model;

            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wizz", ((FizzBuzzModel)viewModel.DivisionList[2]).Message);
                Assert.AreEqual("Wizz", ((FizzBuzzModel)viewModel.DivisionList[5]).Message);
            }
            else
            {
                Assert.AreEqual("Fizz", ((FizzBuzzModel)viewModel.DivisionList[2]).Message);
                Assert.AreEqual("Fizz", ((FizzBuzzModel)viewModel.DivisionList[5]).Message);
            }

            Assert.AreEqual("blue", ((FizzBuzzModel)viewModel.DivisionList[2]).MessageColor);
            Assert.AreEqual("blue", ((FizzBuzzModel)viewModel.DivisionList[5]).MessageColor);
        }

        /// <summary>
        /// Setup method
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.container = new Container(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.Assembly("FizzBuzzBusiness");
                    scan.AddAllTypesOf<BaseFizzBuzz>();
                });
            });
        }
    }
}
