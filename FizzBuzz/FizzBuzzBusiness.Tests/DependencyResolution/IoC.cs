//-----------------------------------------------------------------------
// <copyright file="IoC.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzzBusinessLayer.Tests.DependencyResolution
{
    using FizzBuzzBusinessLayer;
    using StructureMap;
    using StructureMap.Graph;

    /// <summary>
    /// Initializes a new instance of the IoC class.
    /// </summary>
    public static class IoC
    {
        /// <summary>
        /// Initialize container
        /// </summary>
        /// <returns>return container</returns>
        public static IContainer Initialize()
        {
            var container = new Container(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.Assembly("FizzBuzzBusinessLayer");
                    scan.AddAllTypesOf<BaseFizzBuzz>();
                });
            });
            return container;
        }
    }
}