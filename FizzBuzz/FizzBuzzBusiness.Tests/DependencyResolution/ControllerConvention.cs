//-----------------------------------------------------------------------
// <copyright file="ControllerConvention.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------
namespace FizzBuzzBusinessLayer.Tests.DependencyResolution
{
    using System;
    using System.Web.Mvc;

    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;
    using StructureMap.Pipeline;
    using StructureMap.TypeRules;

    /// <summary>
    /// Initializes a new instance of the ControllerConvention class.
    /// </summary>
    public class ControllerConvention : IRegistrationConvention
    {
        #region Public Methods and Operators
        /// <summary>
        /// Process method
        /// </summary>
        /// <param name="type">parameter type</param>
        /// <param name="registry">parameter registry</param>
        public void Process(Type type, Registry registry)
        {
            if (type.CanBeCastTo<Controller>() && !type.IsAbstract)
            {
                registry.For(type).LifecycleIs(new UniquePerRequestLifecycle());
            }
        }

        #endregion
    }
}