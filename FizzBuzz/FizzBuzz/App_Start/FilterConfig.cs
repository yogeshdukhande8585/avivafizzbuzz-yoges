﻿//-----------------------------------------------------------------------
// <copyright file="FilterConfig.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------
namespace FizzBuzz
{
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Definition of FilterConfig class
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// register global filters
        /// </summary>
        /// <param name="filters">input filters</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}