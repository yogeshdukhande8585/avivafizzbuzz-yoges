﻿//-----------------------------------------------------------------------
// <copyright file="RouteConfig.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// Definition of RouteConfig class
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// register routes
        /// </summary>
        /// <param name="routes">input routes</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("Content/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "FizzBuzz", action = "FizzBuzzView", id = UrlParameter.Optional });
        }
    }
}