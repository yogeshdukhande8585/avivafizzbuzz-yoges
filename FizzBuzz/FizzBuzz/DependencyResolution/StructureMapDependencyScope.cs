//-----------------------------------------------------------------------
// <copyright file="StructureMapDependencyScope.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.DependencyResolution
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using Microsoft.Practices.ServiceLocation;

    using StructureMap;

    /// <summary>
    /// The structure map dependency scope.
    /// </summary>
    public class StructureMapDependencyScope : ServiceLocatorImplBase
    {
        #region Constants and Fields
        /// <summary>
        /// Define NestedContainerKey
        /// </summary>
        private const string NestedContainerKey = "Nested.Container.Key";

        #endregion

        #region Constructors and Destructors
        /// <summary>
        /// Initializes a new instance of the StructureMapDependencyScope class.
        /// </summary>
        /// <param name="container">parameter container</param>
        public StructureMapDependencyScope(IContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            this.Container = container;
        }

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets Container
        /// </summary>
        public IContainer Container { get; set; }

        /// <summary>
        /// Gets or sets CurrentNestedContainer
        /// </summary>
        public IContainer CurrentNestedContainer
        {
            get
            {
                return (IContainer)this.HttpContext.Items[NestedContainerKey];
            }

            set
            {
                this.HttpContext.Items[NestedContainerKey] = value;
            }
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets HttpContext
        /// </summary>
        private HttpContextBase HttpContext
        {
            get
            {
                var ctx = this.Container.TryGetInstance<HttpContextBase>();
                return ctx ?? new HttpContextWrapper(System.Web.HttpContext.Current);
            }
        }

        #endregion

        #region Public Methods and Operators
        /// <summary>
        /// Create nested container
        /// </summary>
        public void CreateNestedContainer()
        {
            if (this.CurrentNestedContainer != null)
            {
                return;
            }

            this.CurrentNestedContainer = this.Container.GetNestedContainer();
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            this.DisposeNestedContainer();
            this.Container.Dispose();
        }

        /// <summary>
        /// Dispose Nested Container method
        /// </summary>
        public void DisposeNestedContainer()
        {
            if (this.CurrentNestedContainer != null)
            {
                this.CurrentNestedContainer.Dispose();
                this.CurrentNestedContainer = null;
            }
        }

        /// <summary>
        /// Get Services
        /// </summary>
        /// <param name="serviceType">parameter serviceType</param>
        /// <returns>return IEnumerable</returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this.DoGetAllInstances(serviceType);
        }

        #endregion

        #region Methods
        /// <summary>
        /// Do Get All Instances
        /// </summary>
        /// <param name="serviceType">parameter serviceType</param>
        /// <returns>return IEnumerable</returns>
        protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
        {
            return (this.CurrentNestedContainer ?? this.Container).GetAllInstances(serviceType).Cast<object>();
        }

        /// <summary>
        /// Do get Instance
        /// </summary>
        /// <param name="serviceType">parameter serviceType</param>
        /// <param name="key">parameter Key</param>
        /// <returns>return object</returns>
        protected override object DoGetInstance(Type serviceType, string key)
        {
            IContainer container = this.CurrentNestedContainer ?? this.Container;

            if (string.IsNullOrEmpty(key))
            {
                return serviceType.IsAbstract || serviceType.IsInterface
                    ? container.TryGetInstance(serviceType)
                    : container.GetInstance(serviceType);
            }

            return container.GetInstance(serviceType, key);
        }

        #endregion
    }
}
