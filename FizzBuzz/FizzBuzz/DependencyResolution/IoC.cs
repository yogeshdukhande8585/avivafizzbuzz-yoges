//-----------------------------------------------------------------------
// <copyright file="IoC.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.DependencyResolution
{
    using FizzBuzz.Repository;
    using FizzBuzzBusinessLayer;
    using StructureMap;
    using StructureMap.Graph;

    /// <summary>
    /// Definition of IoC class
    /// </summary>
    public static class IoC
    {
        /// <summary>
        /// Initialize method
        /// </summary>
        /// <returns>return IContainer</returns>
        public static IContainer Initialize()
        {
            var container = new Container(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.Assembly("FizzBuzzBusiness");
                    scan.AddAllTypesOf<BaseFizzBuzz>();
                    scan.Assembly("FizzBuzzRepository");
                    scan.AddAllTypesOf<IRepository>();
                });
            });
            return container;
        }
    }
}