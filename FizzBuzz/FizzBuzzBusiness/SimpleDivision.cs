﻿//-----------------------------------------------------------------------
// <copyright file="SimpleDivision.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzzBusinessLayer
{
    using System;

    /// <summary>
    /// Definition of SimpleDivision class
    /// </summary>
    public class SimpleDivision : BaseFizzBuzz
    {
        /// <summary>
        /// to set message
        /// </summary>
        /// <param name="inputisSpecificDay">input IsSpecificDay</param>
        public override void SetMessage(bool inputisSpecificDay)
        {
            this.Message = this.Number.ToString();
        }

        /// <summary>
        /// Check number is divisible
        /// </summary>
        /// <param name="inputNumber">input Number</param>
        /// <returns>return boolean</returns>
        public override bool IsDivisible(int inputNumber)
        {
            this.Number = inputNumber;
            return true;
        }

        /// <summary>
        /// to set message
        /// </summary>
        public override void SetMessageColor()
        {
            this.MessageColor = string.Empty;
        }
    }
}
