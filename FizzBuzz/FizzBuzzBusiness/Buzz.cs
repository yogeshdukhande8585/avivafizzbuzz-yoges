﻿//-----------------------------------------------------------------------
// <copyright file="Buzz.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzzBusinessLayer
{
    using System;

    /// <summary>
    /// definition of Buzz class
    /// </summary>
    public class Buzz : BaseFizzBuzz
    {
        /// <summary>
        /// check whether number is divisible by 5
        /// </summary>
        /// <param name="inputNumber">the input Number</param>
        /// <returns> the boolean</returns>
        public override bool IsDivisible(int inputNumber)
        {
            return (inputNumber % 5) == 0 ? true : false;
        }

        /// <summary>
        /// to set message
        /// </summary>
        /// <param name="isSpecificDay">the input IsSpecificDay</param>
        public override void SetMessage(bool isSpecificDay)
        {
            this.Message = isSpecificDay ? "Wuzz" : "Buzz";
        }

        /// <summary>
        /// to set message
        /// </summary>
        public override void SetMessageColor()
        {
            this.MessageColor = "green";
        }
    }
}
