﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzBusinessLogic.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzzBusinessLayer
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Definition of FizzBuzzBusinessLogic
    /// </summary>
    public class FizzBuzzBusinessLogic
    {
        /// <summary>
        /// execute business rules
        /// </summary>
        /// <param name="rules">parameter rules</param>
        /// <param name="inputnumber">parameter inputNumber</param>
        /// <returns>return FizzBuzzBusinessRule</returns>
        public BaseFizzBuzz ExecuteBusinessRule(IEnumerable<BaseFizzBuzz> rules, int inputnumber)
        {
            foreach (var rule in rules)
            {
                if (rule.IsDivisible(inputnumber))
                {
                    rule.SetMessage(this.IsSpecificDay());
                    rule.SetMessageColor();
                    BaseFizzBuzz fizzBuzzBusinessRule = new SimpleDivision();
                    fizzBuzzBusinessRule.Message = rule.Message;
                    fizzBuzzBusinessRule.MessageColor = rule.MessageColor;

                    return fizzBuzzBusinessRule;
                }
            }

            return null;
        }

        /// <summary>
        /// get whether today is wednesday
        /// </summary>
        /// <returns>return boolean</returns>
        public bool IsSpecificDay()
        {
            return DayOfWeek.Wednesday == System.DateTime.Now.DayOfWeek ? true : false;
        }
    }
}
